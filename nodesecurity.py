# encoding: utf-8
# ########################################
#  This project works with python 2.7+   #
# ########################################
import os
import sys
import subprocess
import json
import imp
import re
import webbrowser
import tempfile
import datetime

tmpFileHTML = ""

imp.reload(sys)
sys.setdefaultencoding('utf8')

BLACK = lambda x: '\033[30m' + str(x)
RED = lambda x: '\033[31m' + str(x)
GREEN = lambda x: '\033[32m' + str(x)
YELLOW = lambda x: '\033[33m' + str(x)
BLUE = lambda x: '\033[34m' + str(x)
MAGENTA = lambda x: '\033[35m' + str(x)
CYAN = lambda x: '\033[36m' + str(x)
WHITE = lambda x: '\033[37m' + str(x)
UNDERLINE = lambda x: '\033[4m' + str(x)
RESET = lambda x: '\033[0m' + str(x)


def encode(string):
    try:
        return string.decode('utf-8').strip()
    except UnicodeError:
        return string.strip()


def info(message):
    print(encode(BLUE("[INFO] " + message)))


def success(message):
    print(encode(GREEN("[SUCCESS] " + message)))


def error(message):
    print(encode(RED("[ERROR] " + message)))


def warn(message):
    print(encode(YELLOW("[WARN] " + message)))


def log(message):
    print(encode("[LOG] " + message))


def run(cmd):
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    return proc.returncode, stdout, stderr


resume = {
    "npm": {
        "info": 0,
        "low": 0,
        "moderate": 0,
        "high": 0,
        "critical": 0
    },
    "yarn": {
        "info": 0,
        "low": 0,
        "moderate": 0,
        "high": 0,
        "critical": 0
    },
    "snyk": {
        "info": 0,
        "low": 0,
        "moderate": 0,
        "high": 0,
        "critical": 0
    }
}

if os.path.isfile('./package.json') is not True:
    error("Aparentemente este no es un proyecto compatible ya que no existe el archivo package.json")
    sys.exit(1)

if os.path.isfile('./package-lock.json'):
    info("Respaldando archivo package-lock.json")
    os.rename('./package-lock.json', './package-lock.json.bak')
    success("Archivo package-lock-json respaldado")

if os.path.isfile('./yarn.lock'):
    info("Respaldando archivo yarn.lock")
    os.rename('./yarn.lock', './yarn.lock.bak')
    success("Archivo yarn.lock respaldado")

info("Eliminando ./node_modules/")
run("rm -rf ./node_modules")
success("Carpeta node_module eliminada")

snyk = run("snyk --version")
showVersion = run("npm show snyk version")

# Instalando herramienta para generar reporte HTML
run("npm install -g snyk-to-html")

if snyk[0] == 0 and showVersion[0] == 0:
    if snyk[1].decode('utf-8').strip() != showVersion[1].decode('utf-8').strip():
        info("Instalando ultima version de snyk:" + showVersion[1].decode('utf-8').strip())
        run("npm install -g snyk@latest")

info("Leyendo package.json")
with open('./package.json') as json_file:
    data = json.load(json_file)
    if "engines" in data:
        if "node" in data['engines']:
            nodeVersion = re.sub(r"[^\d\.]+", "", data['engines']['node'])
            info("Cambiando la version de nodejs a la " + nodeVersion + " con el comando n")
            run("n " + nodeVersion)
            info("Actualizando version de npm a una version 6 o superior")
            run("npm install -g npm@6")

info("Instalando dependencias con yarn")
run("yarn install")

info("Eliminando dependencias instalandas con Yarn")
run("rm -rf ./node_modules")

info("Instalando dependencias con npm")
run("npm install")

info("Auditando con npm")
npmAudit = run("npm audit --json")

info("Auditando con yarn")
yarnAudit = run("yarn audit --json")

info("Auditando con snyk")
snykAudit = run("snyk test --json")

info("Generando reporte html")

tmpFileHTML = tempfile.gettempdir() + "/vulnerabilities-" + datetime.date.today().strftime("%Y%m%d%H%M%S") + ".html"

run("snyk test --json | snyk-to-html -o " + tmpFileHTML)
success("Reporte HTML generado exitosamente: file://" + tmpFileHTML)

npmAudited = json.loads(npmAudit[1])
resume["npm"]["info"] = npmAudited["metadata"]["vulnerabilities"]["info"]
resume["npm"]["low"] = npmAudited["metadata"]["vulnerabilities"]["low"]
resume["npm"]["moderate"] = npmAudited["metadata"]["vulnerabilities"]["moderate"]
resume["npm"]["high"] = npmAudited["metadata"]["vulnerabilities"]["high"]
resume["npm"]["critical"] = npmAudited["metadata"]["vulnerabilities"]["critical"]

yarnAudited = json.loads(yarnAudit[1])
resume["yarn"]["info"] = yarnAudited["data"]["vulnerabilities"]["info"]
resume["yarn"]["low"] = yarnAudited["data"]["vulnerabilities"]["low"]
resume["yarn"]["moderate"] = yarnAudited["data"]["vulnerabilities"]["moderate"]
resume["yarn"]["high"] = yarnAudited["data"]["vulnerabilities"]["high"]
resume["yarn"]["critical"] = yarnAudited["data"]["vulnerabilities"]["critical"]

snykAudited = json.loads(snykAudit[1])
i = 0
while i < len(snykAudited["vulnerabilities"]):
    item = snykAudited["vulnerabilities"][i]
    severity = item["severity"]

    if severity == "medium":
        severity = "moderate"
    resume["snyk"][severity] += 1
    i += 1

info(json.dumps(resume, indent=4))

webbrowser.open("file://" + tmpFileHTML, new=2, autoraise=True)
