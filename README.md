# Simple NPM package audit
This project combine `npm audit`, `yarn audtir` and `snyk test` to fetch all vulnerabilities.

## Requeriments
- [x] Python 2.7+
- [ ] OS
  - [x] OSX
  - [x] Ubuntu
  - [ ] Windows 7+
- [x] n
- [x] npm
- [x] yarn
- [x] snyk


## Run:
```
python <(curl -s https://gitlab.com/olaferlandsen/package-simple-audit/raw/master/nodesecurity.py)
```

